import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

declare var bridgefy: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // bridgefy.initialize((response) => {
      //   console.log('Bridgefy client: ' + JSON.stringify(response));
      // }, err => {
      //   console.error(err);
      // }, '49d9ad3f-e586-4fb0-b31e-afde5c1386e6');

    //   bridgefy.initialize((response) => { // successCallback
    //             console.log('Bridgefy client: ' + JSON.stringify(response));
    //         }, (e) => { // errorCallback
    //           console.error(e);
    //         }, '49d9ad3f-e586-4fb0-b31e-afde5c1386e6');
    });
  }
}
