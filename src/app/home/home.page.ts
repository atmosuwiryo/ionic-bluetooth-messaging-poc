import { Component, OnDestroy, ChangeDetectorRef } from '@angular/core';



declare var bridgefy: any;

interface Chat {
  userId?: string;
  message?: string;
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnDestroy {
  userChat: string;
  chat: Chat = {};
  chats: Chat[] = [];
  device: any;
  action: any;
  userUuid: any;

  constructor(private ref: ChangeDetectorRef) {
      bridgefy.initialize((response) => {
        console.log('Bridgefy client: ' + JSON.stringify(response));
        this.userUuid = response.userUuid;
        this.bridgefyRoutine();
      }, err => {
        console.error(err);
      }, '49d9ad3f-e586-4fb0-b31e-afde5c1386e6');
  }

  bridgefyRoutine() {
    // Add Device Listener
    bridgefy.deviceListener((response) => {
        // response has a action and device
        // response = {
        //  action: "STATUS", -> Must be "connected" or "disconnected"
        //  device: Object
        // }
        this.action = response.action;
        this.device = response.device;

        console.log(JSON.stringify(response));
        this.chats.push({userId: response.device.userId.split('-')[0], message: response.action});
        this.ref.detectChanges();
    }, (e) => {
        console.error(e);
    });

    // Add Message Listener
    bridgefy.messageListener((response) => { // successCallback
      console.log('Receive message event: ' + response.action);
      console.log('Receive message content: ' + JSON.stringify(response.message));
      this.chats.push({userId: response.message.senderId.split('-')[0], message: response.message.content.message});
      this.ref.detectChanges();
    }, (e) => { // errorCallback
      console.error(e);
    });

    // Start operations
    bridgefy.start(() => {
      console.log ('Bridgefy started, scanning and advertising device');
    }, err => {
      console.log('Bridgefy error ', JSON.stringify(err));
    });
  }

  ngOnDestroy() {
    bridgefy.stop(() => {
      // Your code
    }, err => {
      console.error(err);
    });
  }

  sendChat() {
    this.chats.push({message: this.userChat});
    this.sendBroadcastMsg(this.userChat);
    delete this.userChat;
  }

  sendMsg(device) {
    bridgefy.sendMessage((res) => {    // Success Callback
          console.log('Message sent: ' + res);
      }, (e) => { // Error Callback
          console.error('Error to send message: ', e);
      },
      device, // Sent to device
      { message: 'Test' }); // Message
  }

  sendBroadcastMsg(msg) {
    bridgefy.sendBroadcastMessage((res) => {    // Success Callback
          console.log('Message sent: ' + res);
      }, (e) => { // Error Callback
          console.error('Error to send message: ', e);
      },
      { message: msg }); // Message
  }
}
